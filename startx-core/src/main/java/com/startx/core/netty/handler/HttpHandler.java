package com.startx.core.netty.handler;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.apache.log4j.Logger;

import com.startx.core.config.ConfigHolder;
import com.startx.core.mvc.define.BodyType;
import com.startx.core.mvc.factory.MappingFactory;
import com.startx.core.netty.output.http.JsonOutput;
import com.startx.core.netty.output.http.Output;
import com.startx.core.netty.output.http.XmlOutput;
import com.startx.core.system.config.StartxConfig;
import com.startx.core.system.constants.Constants;
import com.startx.core.system.model.AccessTarget;
import com.startx.core.system.param.HttpBody;
import com.startx.core.system.param.HttpContext;
import com.startx.core.system.param.HttpHeader;
import com.startx.core.system.param.HttpParam;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.QueryStringDecoder;

public class HttpHandler extends ChannelInboundHandlerAdapter {

	private static final Logger Log = Logger.getLogger(HttpHandler.class);
	
	private StartxConfig config = ConfigHolder.getConfig();

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		FullHttpRequest req = (FullHttpRequest) msg;
		handleHttpRequest(ctx, req);
	}

	/**
	 * 解析http请求
	 */
	public void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {

		if (request.decoderResult().isFailure()) {
			Log.error("一次奇怪的访问， IP：" + ctx.channel().remoteAddress() + "... ");
			ctx.close();
			return;
		}
		//GET POST ...
		AccessTarget target = getTarget(ctx, request);
		
		if(Objects.isNull(target)) {
			Output.status(ctx, HttpResponseStatus.NOT_FOUND);
			return;
		}
		
		BodyType bodyType = target.getType();
		// 判断是否需要解析body
		Method method = target.getMethod();
		Parameter[] parameters = method.getParameters();
		Object[] params = new Object[parameters.length];
		
		//设置方法调用参数
		setParams(ctx, request, bodyType, parameters, params);
		// 调用方法
		Object output = method.invoke(target.getObj(),params);
		// 根据类型写返回值
		ifReturnValueThenOutput(ctx, bodyType, output);
		// 检测链接是否关闭
		ifIsOpenThenClose(ctx);
	}

	/**
	 * 设置http参数
	 * @param ctx
	 * @param request
	 * @return
	 */
	private HttpParam getHttpParams(ChannelHandlerContext ctx, FullHttpRequest request) {
		return new HttpParam(getParams(request));
	}

	/**
	 * 设置方法调用参数
	 * @param ctx
	 * @param request
	 * @param bodyType
	 * @param parameters
	 * @param params
	 * @throws Exception
	 */
	private void setParams(ChannelHandlerContext ctx, FullHttpRequest request, BodyType bodyType,
						   Parameter[] parameters, Object[] params) throws Exception {
		//传参，可以自行扩展
		
		for(int i = 0;i < parameters.length;i++) {
			
			Parameter parameter = parameters[i];
			Class<?> type = parameter.getType();
			
			if(type.equals(HttpContext.class)) {
				params[i] = getHttpContext(ctx,request);
			}
			
			if(type.equals(HttpParam.class)) {
				params[i] = getHttpParams(ctx, request);
			}
			
			if(type.equals(HttpBody.class)) {
				params[i] = getBody(request,bodyType);
			}
			
			if(type.equals(HttpHeader.class)) {
				params[i] = getHeader(request);
			}
			
		}
	}

	/**
	 * 获取Http上下文环境
	 * @param ctx
	 * @param request
	 * @return
	 */
	private HttpContext getHttpContext(ChannelHandlerContext ctx, FullHttpRequest request) {
		return new HttpContext(getIp(ctx), ctx);
	}

	/**
	 * 获得调用对象
	 * @param ctx
	 * @param request
	 * @return
	 */
	private AccessTarget getTarget(ChannelHandlerContext ctx, FullHttpRequest request) {
		String httpMethod = request.method().name();
		String uri = getURI(request);
		String endPoint = config.getEndPoint();
		
		if(!Objects.isNull(endPoint) && !uri.startsWith(endPoint)) {
			return null;
		}
		
		if(!endPoint.equals(Constants.ROOT_DIR)) {
			uri = uri.replace(endPoint, Constants.EMPTY_STRING);
		}
		
		AccessTarget target = 
				MappingFactory.getAccessTarget(httpMethod+Constants.UNDER_LINE+uri);
		
		return target;
	}

	/**
	 * 获取header参数
	 */
	private HttpHeader getHeader(FullHttpRequest request) {
		Map<String, String> headers = new HashMap<String, String>();

		List<Map.Entry<String, String>> entryList = request.headers().entries();
		for (Map.Entry<String, String> entry : entryList) {
			String key = entry.getKey();
			headers.put(key, entry.getValue());
		}

		return new HttpHeader(headers);
	}

	/**
	 * 解析body
	 */
	private HttpBody getBody(FullHttpRequest request, BodyType bodyType) {
		return new HttpBody(request.content(),bodyType);
	}

	/**
	 * Get获取uri参数
	 */
	private Map<String, String> getParams(FullHttpRequest request) {
		Map<String, String> params = new HashMap<String, String>();

		QueryStringDecoder decoder = new QueryStringDecoder(request.uri());

		Map<String, List<String>> parame = decoder.parameters();
		for (Entry<String, List<String>> entry : parame.entrySet()) {
			params.put(entry.getKey(), entry.getValue().get(0));
		}
		
		return params;
	}

	/**
	 * 输出数据
	 */
	private void ifReturnValueThenOutput(ChannelHandlerContext ctx, BodyType bodyType, Object output)
			throws UnsupportedEncodingException {
		if (output != null) {
			if (bodyType == BodyType.JSON) {
				JsonOutput.object(ctx, HttpResponseStatus.OK, output);
			} else if (bodyType == BodyType.XML) {
				XmlOutput.object(ctx, HttpResponseStatus.OK, output);
			}
		}
	}

	/**
	 * 获取客户端IpAddress
	 */
	private String getIp(ChannelHandlerContext ctx) {
		return ctx.channel().remoteAddress().toString().replaceAll(Constants.ROOT_DIR, Constants.EMPTY_STRING)
				.split(Constants.COLON)[0];
	}

	/**
	 * 获取请求URI
	 */
	private String getURI(FullHttpRequest request) {
		String requestUri = request.uri();

		if (requestUri.indexOf(Constants.PARAM_SPLIT) != -1) {
			requestUri = requestUri.substring(0, request.uri().indexOf(Constants.PARAM_SPLIT));
		}

		return requestUri;
	}

	/**
	 * 如果链接没有关闭，则执行关闭
	 */
	private void ifIsOpenThenClose(ChannelHandlerContext ctx) {
		if (ctx.channel().isOpen()) {
			Output.status(ctx, HttpResponseStatus.OK);
		}
	}

	/**
	 * 异常处理
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		Output.status(ctx, HttpResponseStatus.INTERNAL_SERVER_ERROR);
	}
}

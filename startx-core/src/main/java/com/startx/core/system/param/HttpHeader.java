package com.startx.core.system.param;

import java.util.Map;

/**
 * http请求头
 * @author minghu.zhang
 */
public class HttpHeader {
	
	/**
	 * 请求头
	 */
	private Map<String,String> headers;
	
	public HttpHeader(Map<String,String> headers) {
		this.headers = headers;
	}

	/**
	 * 获取参数表
	 * @return
	 */
	public Map<String, String> getHeaders() {
		return headers;
	}

	/**
	 * 获取单个参数
	 * @param key
	 * @return
	 */
	public String get(String key) {
		return headers.get(key);
	}
}

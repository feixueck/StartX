package com.startx.core.system.model;

import java.lang.reflect.Method;

import com.startx.core.mvc.define.BodyType;

/**
 * 调用目标对象
 */
public class AccessTarget {
	/**
	 * JavaMethod
	 */
	private Method method;
	/**
	 * 调用对象
	 */
	private Object obj;
	/**
	 * 响应类型
	 */
	private BodyType type;

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public BodyType getType() {
		return type;
	}

	public void setType(BodyType type) {
		this.type = type;
	}
	
}

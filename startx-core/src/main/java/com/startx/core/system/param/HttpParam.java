package com.startx.core.system.param;

import java.util.Map;

/**
 * http请求参数
 */
public class HttpParam {
	/**
	 * URI 参数
	 */
	private Map<String, String> params;
	
	public HttpParam(Map<String, String> params) {
		this.params = params;
	}

	/**
	 * 获取参数表
	 * @return
	 */
	public Map<String, String> getParams() {
		return params;
	}
	
	/**
	 * 获取单个参数
	 * @param key
	 * @return
	 */
	public String get(String key) {
		return params.get(key);
	}
}

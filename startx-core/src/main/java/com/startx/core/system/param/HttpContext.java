package com.startx.core.system.param;

import io.netty.channel.ChannelHandlerContext;

/**
 * HTTP上下文
 * 
 * @author minghu.zhang
 */
public class HttpContext {
	/**
	 * ip地址
	 */
	private String remoteAddress;
	/**
	 * 管道上下文
	 */
	private ChannelHandlerContext channelHandlerContext;

	public HttpContext(String remoteAddress, ChannelHandlerContext channelHandlerContext) {
		super();
		this.remoteAddress = remoteAddress;
		this.channelHandlerContext = channelHandlerContext;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public ChannelHandlerContext getChannelHandlerContext() {
		return channelHandlerContext;
	}

}
